$(document).ready(function () {
    // TODO: Your code here
    $('#choose-background').change(function() {
        var chosen = $('#choose-background input:checked').attr('id');
        var changeID = chosen.substring(6,17);
        var gif;
        if(changeID==='background6'){
           gif = ".gif"
        }else{
            gif = ".jpg"
        }$('#background').attr('src',"../images/"+changeID+gif);
    });

    $('#toggle-components').change(function() {
        $('#toggle-components input').each(function() {

            var displayDolphin = $(this).attr("id").substring(6, 14);

            if ($(this).is(":checked")) {
                $('#' + displayDolphin).show();
            }else {
                $('#' + displayDolphin).hide();
            }
        })
    }).change();

    $('#vertical-control').on('input', function(){
        $('.dolphin').css('transform', 'translateY('+$(this).val()+'px)');
    });

    $('#horizontal-control').on('input', function(){
        $('.dolphin').css('transform', 'translateX('+$(this).val()+'px)');
    });

    $('#size-control').on('input', function() {
        $('.dolphin').css('transform', 'scale(' + (1 + ($(this).val()/100))+')');
    })
    });
